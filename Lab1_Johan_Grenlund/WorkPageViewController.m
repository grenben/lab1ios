//
//  WorkPageViewController.m
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-29.
//  Copyright © 2016 johan. All rights reserved.
//

#import "WorkPageViewController.h"

@interface WorkPageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *aboutWorkLabel;

@property (weak, nonatomic) IBOutlet UIImageView *workImage;

@property (nonatomic) int imageArrayIndex;

@property (nonatomic, copy) NSArray *image;

@property (nonatomic, copy) NSArray *aboutWorkText;


- (IBAction)nextImageButton:(id)sender;

@end

@implementation WorkPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.image = [[NSArray arrayWithObjects:
                   [UIImage imageNamed: @"work1.jpg"],
                   [UIImage imageNamed: @"work2.jpg"],
                   [UIImage imageNamed: @"work3.jpg"],
                   [UIImage imageNamed: @"work4.jpg"],
                   [UIImage imageNamed: @"work5.jpg"],
                   [UIImage imageNamed: @"work6.jpg"],
                   [UIImage imageNamed: @"work7.jpg"],
                   [UIImage imageNamed: @"work8.jpg"],
                   [UIImage imageNamed: @"fusion.jpg"],
                   [UIImage imageNamed: @"work10.jpg"],nil] init];
    
    self.aboutWorkText = @[@"Profile shoot of personal trainer Oskar. Customer: Crossfit Bislett, 2014.",
                           @"Stilleben shoot in my studio. Own work, 2012.",
                           @"Closeup of custom made visuals for Eon Hood on tour, Customer: Eon Hood, 2013.",
                           @"Projektion design in Malmö, Customer: Hyresgästföreningen, 2012.",
                           @"Closeup of custom made visuals for Eon Hood on tour, Customer: Eon Hood, 2014.",
                           @"Closeup of custom made visuals for Eon Hood on tour, Customer: Eon Hood, 2013.",
                           @"Installation The Mushroom @ Sjönevadsfesten. Customer: Sjönevadsfesten, 2014.",
                           @"Installation The Mushroom @ Sjönevadsfesten. Customer: Sjönevadsfesten, 2014.",
                           @"Installation The Carusel @ Fusion, Germany. Customer: Fusion, 2015.",
                           @"Profile shoot of personal trainer Mette, Customer: Girls fithness, 2012."];
    
    self.imageArrayIndex = 1;
    
    self.workImage.image = self.image[0];
    self.aboutWorkLabel.text = self.aboutWorkText[0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextImageButton:(id)sender {
    
    if (self.imageArrayIndex == 10) {
        
        self.imageArrayIndex = 0;
        self.aboutWorkLabel.text = self.aboutWorkText[self.imageArrayIndex];
        self.workImage.image = self.image[self.imageArrayIndex];
        self.imageArrayIndex ++;
    }
    else
    {
        self.aboutWorkLabel.text = self.aboutWorkText[self.imageArrayIndex];
        self.workImage.image = self.image[self.imageArrayIndex];
        self.imageArrayIndex ++;
    }
}









@end
