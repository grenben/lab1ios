//
//  AboutMePageViewController.m
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-29.
//  Copyright © 2016 johan. All rights reserved.
//

#import "AboutMePageViewController.h"

@interface AboutMePageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *aboutMeLabel;

@property (weak, nonatomic) IBOutlet UISlider *redColorSlider;

@property (weak, nonatomic) IBOutlet UISlider *grenColorSlider;

@property (weak, nonatomic) IBOutlet UISlider *blueColorSlider;

@property (weak, nonatomic) IBOutlet UIView *background;
@end

@implementation AboutMePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.background.backgroundColor = [self mixedColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIColor *) mixedColor {
    return [UIColor colorWithRed:self.redColorSlider.value
                           green:self.grenColorSlider.value
                            blue:self.blueColorSlider.value
                           alpha:1.0f];
}

- (IBAction)colorSliderChanged:(UISlider *)sender {
    self.background.backgroundColor = [self mixedColor];
    
}

@end
