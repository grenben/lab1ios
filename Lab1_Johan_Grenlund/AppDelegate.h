//
//  AppDelegate.h
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-28.
//  Copyright © 2016 johan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

