//
//  ViewController.m
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-28.
//  Copyright © 2016 johan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.backgroundImage.image = [UIImage imageNamed:@"fusion.jpg"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
