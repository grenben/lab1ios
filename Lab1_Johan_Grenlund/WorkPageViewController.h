//
//  WorkPageViewController.h
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-29.
//  Copyright © 2016 johan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkPageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *nextButtonClicket;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end
