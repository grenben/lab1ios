//
//  MePageViewController.m
//  Lab1_Johan_Grenlund
//
//  Created by ITHS on 2016-01-29.
//  Copyright © 2016 johan. All rights reserved.
//

#import "MePageViewController.h"

@interface MePageViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

- (IBAction)nextSelf:(UIButton *)sender;

@property (nonatomic) int imageArrayIndex;

@property (nonatomic, copy) NSArray *image;



@end

@implementation MePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.image = [[NSArray arrayWithObjects:
                   [UIImage imageNamed: @"johan1.jpg"],
                   [UIImage imageNamed: @"johan2.jpg"],
                   [UIImage imageNamed: @"johan3.jpg"],
                   [UIImage imageNamed: @"johan4.jpg"], nil] init];
    self.imageArrayIndex = 1;
    
    self.backgroundImage.image = self.image[0];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nextSelf:(UIButton *)sender {
    
    if (self.imageArrayIndex == 4)
    {
        self.imageArrayIndex = 0;
        self.backgroundImage.image = self.image[self.imageArrayIndex];
        self.imageArrayIndex ++;
    }
    else
    {
        self.backgroundImage.image = self.image[self.imageArrayIndex];
        self.imageArrayIndex ++;
    }
}




@end
